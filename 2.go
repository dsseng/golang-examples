package main

import (
	"fmt"
	"sync"
)

func main() {
	a := [5]int64{2, 4, 6, 8, 10}
	wg := sync.WaitGroup{}

	wg.Add(len(a))
	for _, x := range a {
		go func(i int64) {
			fmt.Println(i * i)
			wg.Done()
		}(x)
	}

	wg.Wait()
}
