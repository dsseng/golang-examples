package main

import (
	"fmt"
	"time"
)

func main() {
	in := make(chan int)
	out := make(chan int)
	arr := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}

	go func(in chan int) {
		for x := range arr {
			in <- x
			time.Sleep(time.Millisecond * 250)
		}
		close(in)
		fmt.Println("tx exiting")
	}(in)

	go func(in chan int, out chan int) {
		for x := range in {
			out <- x * x
			time.Sleep(time.Millisecond * 250)
		}
		close(out)
		fmt.Println("proc exiting")
	}(in, out)

	for v := range out {
		fmt.Println("received", v)
	}
	fmt.Println("rx exiting")
}
