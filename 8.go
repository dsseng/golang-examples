package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	x_str := "255"
	if len(os.Args) > 1 {
		x_str = os.Args[1]
	}
	x, err := strconv.ParseInt(x_str, 10, 64)
	if err != nil {
		fmt.Println("failed to parse x:", err)
		os.Exit(1)
	}

	i_str := "3"
	if len(os.Args) > 1 {
		i_str = os.Args[2]
	}
	i, err := strconv.ParseInt(i_str, 10, 32)
	if err != nil {
		fmt.Println("failed to parse i:", err)
		os.Exit(1)
	}
	if i < 0 && i > 63 {
		fmt.Println("bad i", i)
		os.Exit(2)
	}

	v_str := "0"
	if len(os.Args) > 1 {
		v_str = os.Args[3]
	}
	v, err := strconv.ParseInt(v_str, 10, 32)
	if err != nil {
		fmt.Println("failed to parse v:", err)
		os.Exit(1)
	}

	mask := int64(1) << i

	if v != 0 {
		x = x | mask
	} else {
		x = x & ^mask
	}

	fmt.Println(x)
}
