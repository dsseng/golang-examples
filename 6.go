package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	quit := make(chan bool)
	ch := make(chan int)
	wg := sync.WaitGroup{}

	wg.Add(2)
	go func(ch chan int) {
		defer wg.Done()
		for v := range ch {
			fmt.Println("value", v)
		}
		fmt.Println("exiting a")
	}(ch)
	go func(ch chan int, quit chan bool) {
		defer wg.Done()
		for i := 0; i >= 0; i++ {
			select {
			case <-quit:
				fmt.Println("exiting b")
				return
			default:
				fmt.Println(i)
				ch <- i
				time.Sleep(1 * time.Second)
			}
		}
	}(ch, quit)

	time.Sleep(3 * time.Second)
	quit <- true
	time.Sleep(3 * time.Second)
	close(ch)

	wg.Wait()
}
